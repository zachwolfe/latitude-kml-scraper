
var globalLocations = new Array(); // Holds all requests
var execute = authorize;

function storeLocations(request)
{
    globalLocations = globalLocations.concat(request.items)
}

// Display message to element by id
function updateElementHtml( elementId, html )
{
    var element = document.getElementById(elementId)
    element.innerHTML = html;
}

// Display status message
function status(msg) 
{
    var element = document.getElementById('statusElement');
    element.innerHTML = element.innerHTML + "<br />" +  msg;
}

function log(msg)
{
    console.log(msg);
}


function authorize() 
{
    status('Configuring');
    var config = 
    {
        'client_id': '200285493690-mucv4kpol4t0b05jda9eurb0k73doag2.apps.googleusercontent.com',
        'scope': 'https://www.googleapis.com/auth/latitude.all.best'
    };

    status('Authorizing with Google API');
    var authorized = false;
    gapi.auth.authorize(config, function() 
    {
        status('Authorization succeeded');
        // Next click of the globe starts grabbing requests!
        updateElementHtml('executeButton', 'Fetch Locations');
        execute = startRequests;
    });
}

function startRequests() 
{
    status('Fetching location from Google Latitude');
    var locations = new Array();

    gapi.client.load('latitude', 'v1', function() 
    {
        var request = gapi.client.latitude.location.list(
            {
                'max-results' : '1000',
                'granularity' : 'best',
            }
        );
        executeRequest(request);
    });
};

// Form a request object based on the previous response's timestamp
// unless the response is empty (i.e. we have all data available from the server)
function makeRequest(response)
{
    var oldestTimestamp = 0;
    if (response==undefined) 
    {
        log("response is undefined");
        return;
    }
    if (response.items == undefined)
    {
        log("response.items is undefined");
        return;
    }
   

    updateElementHtml('locationCounter', globalLocations.length + " locations");
    if (response.items.length == 1)
    {
        status("Finished fetching locations");  
        addDownloadify(getKmlFileAsString(globalLocations));
        return;
    }

    response.items.map(
        function(item) 
        {
            if (oldestTimestamp == 0) 
            {
                oldestTimestamp = item.timestampMs;
            }
            else 
            {
                oldestTimestamp = Math.min(oldestTimestamp, item.timestampMs); 
            }
        }
    );

    // Construct a request with 'max-time' parameter 
    // equal to the oldest timestamp from the last request.
    // This will 'paginate' the requests until all data is retrieved.
    var request = gapi.client.latitude.location.list(
        {
            'max-results' : '1000',
            'granularity' : 'best',
            'max-time' : oldestTimestamp
        }
    );

    executeRequest(request)
}

// Execute a new request
function executeRequest(request)
{
    request.execute(
        function(resp)
        {
            storeLocations(resp);
            makeRequest(resp);
        }
    );
}



function getKmlFileAsString(locations)
{
    var xw = new XMLWriter('UTF-8', '1.0');
    xw.formatting = 'indented';
    xw.indentChar = ' ';
    xw.indentation = 4;
    xw.writeStartDocument(false);
    xw.writeEndDocument();
    xw.writeStartElement('kml');
        xw.writeAttributeString('xmlns', 'http://www.opengis.net/kml/2.2');
        xw.writeAttributeString('xmlns:gx', 'http://www.google.com/kml/ext/2.2');
        xw.writeAttributeString('xmlns:kml', 'http://www.opengis.net/kml/2.2');
        xw.writeAttributeString('xmlns:atom', 'http://www.w3.org/2005/Atom');
        xw.writeStartElement('Document');
            xw.writeElementString('name', 'Location history');
            xw.writeElementString('open', '1');
            xw.writeStartElement('Placemark');
                xw.writeElementString('name', 'Latitude user');
                xw.writeElementString('description', 'Location history');
                xw.writeStartElement('gx:Track');
                    xw.writeElementString('altitudeMode', 'clampToGround');
                    
                    var locationCounter = 0;
                    // Write all locations in 'locations' array to xw using helper function
                    locations.map(
                        function(location)
                        {
                            var locationDate = new Date(Number(location.timestampMs));
                            var kmlWhenString = '';
                            kmlWhenString = kmlWhenString + locationDate.getFullYear() + '-';
                            kmlWhenString = kmlWhenString + locationDate.getMonth() + 1 + '-';
                            kmlWhenString = kmlWhenString + locationDate.getDate() + 'T';
                            kmlWhenString = kmlWhenString + locationDate.getHours() + ':';
                            kmlWhenString = kmlWhenString + locationDate.getMinutes() + ':';
                            kmlWhenString = kmlWhenString + locationDate.getSeconds() + '.';
                            kmlWhenString = kmlWhenString + locationDate.getMilliseconds() + ':00';

                            xw.writeElementString('when', kmlWhenString);
                            delete(locationDate);

                            // longitude latitude altitude
                            var kmlCoordString = '';
                            kmlCoordString = kmlCoordString + location.longitude + ' ';
                            kmlCoordString = kmlCoordString + location.latitude + ' ';

                            if (location.altitude == undefined) 
                            {
                                kmlCoordString = kmlCoordString + '0' ;
                            }
                            else
                            {
                                kmlCoordString = kmlCoordString + location.altitude ;
                            }

                            xw.writeElementString('gx:coord', kmlCoordString);
                            locationCounter = locationCounter + 1;
                            if (locationCounter % 100 == 0) 
                            {
                                log('locations map function locaton.timestampMs = ' + location.timestampMs);
                                log('locations map function locationDate = ' + locationDate);
                                log('procesed ' + locationCounter + ' locations');
                            }
                        }
                    );
                xw.writeEndElement(); // track
            xw.writeEndElement(); // placemark
        xw.writeEndElement(); // document
    xw.writeEndElement(); // kml
    xw.writeEndDocument();
    log('xml creation done');
    var xmlDocumentString = xw.flush();
    log('xml flush done');
    updateElementHtml('kmlDump', xmlDocumentString);
    return xmlDocumentString;
}

// add downloadify button
function addDownloadify(kmlString)
{
  Downloadify.create('downloadify',{
    filename: function(){
      return document.getElementById('filename').value;
    },
    data: function(){ 
        log('in data callback ' + kmlString);
        return kmlString;
    },
    onComplete: function(){ 
      alert('Your File Has Been Saved!'); 
    },
    onCancel: function(){ 
      alert('You have cancelled the saving of this file.');
    },
    onError: function(){ 
      alert('You must put something in the File Contents or there will be nothing to save!'); 
    },
    transparent: false,
    swf: 'downloadify.swf',
    downloadImage: 'download.png',
    width: 100,
    height: 30,
    transparent: true,
    append: false
  });
}
























