#!/usr/bin/python2.4
# -*- coding: utf-8 -*-
#
# Copyright (C) 2012 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#            http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Note, parts of this file have been taken from the Google App Engine API
__author__ = 'wolfe.zach@gmail.com (Zach Wolfe)'

import gflags
import httplib2
import os
import sys
import time
import json


from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run


FLAGS = gflags.FLAGS
CLIENT_SECRETS = 'client_secrets.json'

MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

     %s

with information from the APIs Console <https://code.google.com/apis/console>.

""" % os.path.join(os.path.dirname(__file__), CLIENT_SECRETS)

# Set up a Flow object to be used if we need to authenticate.
FLOW = flow_from_clientsecrets(CLIENT_SECRETS,
        scope='https://www.googleapis.com/auth/latitude.all.best',
        message=MISSING_CLIENT_SECRETS_MESSAGE)

def main(argv):
    # If the Credentials don't exist or are invalid run through the native client
    # flow. The Storage object will ensure that if successful the good
    # Credentials will get written back to a file.
    storage = Storage('latitude.dat')
    credentials = storage.get()

    if credentials is None or credentials.invalid:
        credentials = run(FLOW, storage)

    # Create an httplib2.Http object to handle our HTTP requests and authorize it
    # with our good Credentials.
    http = httplib2.Http()
    http = credentials.authorize(http)

    service = build("latitude", "v1", http=http)

        # min_time set to the release date of google latitude (feb 4th 2009) -- no timestamps can be before this date (right?)
    max_time=1233705600000
    locationGranularity="best" # can also be "best"
    lastTimeStamp=0
    searchingDebug=False
    latitudeList = []
    longitudeList = []
    timestampList = []
    try:
        while (True):
            responseMap = service.location().list(granularity=locationGranularity, max_time=max_time, max_results="10000" ).execute()
            if u'items' in responseMap:
                for location in responseMap[u'items']: 
                    timestampMs = location[u'timestampMs']
                    latitude = location[u'latitude']
                    longitude = location[u'longitude']
    
                    timestampList.append(timestampMs)
                    latitudeList.append(latitude)
                    longitudeList.append(longitude)
    
                    print str(timestampMs) + " " + str(latitude) + ", " + str(longitude)
                    
                lastTimeStamp = responseMap[u'items'][len(responseMap[u'items']) - 1][u'timestampMs']
                # if the last time stamp in this response is the same as the last, then that must be the last location object that google has
                if (lastTimeStamp != max_time):
                    max_time = lastTimeStamp
                    print "max_time = lastTimeStamp"
                else:
                    break
            else:
                if max_time < 1533705600000:
                    max_time = max_time + 1000000000
                    if (not searchingDebug): 
                        print "searching for start time"
                        searchingDebug = True
                else: 
                    break

        print "found " + str(len(timestampList)) + " records"



    except AccessTokenRefreshError:
        print ("The credentials have been revoked or expired, please re-run"
            "the application to re-authorize")

if __name__ == '__main__':
    main(sys.argv)
